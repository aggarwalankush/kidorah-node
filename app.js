/**
 * Module dependencies.
 */
const express = require('express');
const compression = require('compression');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const Promise = require('bluebird');
const winston = require('winston');
const mkdirp = require('mkdirp');

/**
 * Load environment variables from .env file
 */
dotenv.load({path: '.env'});

/**
 * Route handlers
 */
const routes = require('./server/routes/index');

/**
 * Create Express server.
 */
const app = express();

/**
 * Winston Configuration
 */
mkdirp.sync(process.env.LOG_DIRECTORY);
winston.add(require('winston-daily-rotate-file'), {
  filename: path.join(process.env.LOG_DIRECTORY, process.env.LOG_FILENAME)
});
winston.remove(winston.transports.Console);

/**
 * Mongoose Configuration
 */
mongoose.Promise = Promise;
mongoose.connect(process.env.MONGODB_URI);
mongoose.connection.on('error', () => {
  console.error('MongoDB Connection Error. Please make sure that MongoDB is running.');
  process.exit(1);
});


/**
 * Express configuration.
 */
app.use(compression());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());
app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, x-access-token');
  res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PATCH, DELETE');
  next();
});
app.use('/api/v1', routes);

/**
 * Error Handler.
 */
app.use(function (err, req, res, next) {
  if (res.statusCode === undefined || res.statusCode === 200) {
    res.status(500);
  }
  return res.json({
    success: false,
    error: err.message
  });
});

// catch 404 and forward to error handler
app.use((req, res, next)=> {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use((err, req, res, next)=> {
    res.status(err.status || 500);
    res.json({
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use((err, req, res, next)=> {
  res.status(err.status || 500);
  res.json({
    message: err.message,
    error: {}
  });
});

module.exports = app;
