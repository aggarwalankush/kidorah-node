const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const mongoosePaginate = require('mongoose-paginate');
const mongooseUniqueValidator = require('mongoose-unique-validator');


const teamSchema = new mongoose.Schema({
  name: {type: String, required: true, unique: true},
  admin_email: {type: String, required: true},
  member_email: [{type: String, required: true}],
  status: {type: String, enum: ['new', 'approved', 'rejected'], required: true, default: 'new'},
  product_allowance: {type: Number, required: true, default: 10}
}, {timestamps: true});

autoIncrement.initialize(mongoose.connection);
teamSchema.plugin(autoIncrement.plugin, {model: 'Team', startAt: 1});
teamSchema.plugin(mongoosePaginate);
teamSchema.plugin(mongooseUniqueValidator);

const Team = mongoose.model('Team', teamSchema);

module.exports = Team;
