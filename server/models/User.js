const bcrypt = require('bcrypt');
const crypto = require('crypto');
const Promise = require('bluebird');
const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const mongooseUniqueValidator = require('mongoose-unique-validator');
const comparePassword = Promise.promisify(bcrypt.compare);

const userSchema = new mongoose.Schema({
  name: {type: String, required: true},
  email: {type: String, required: true, unique: true},
  role: {type: String, enum: ['siteadmin', 'admin', 'member'], required: true},
  username: {type: String, required: true, unique: true},
  password: String,
  passwordResetToken: String,
  passwordResetExpires: Date
}, {timestamps: true});

/**
 * Password hash middleware.
 */
userSchema.pre('save', function (next) {
  const user = this;
  if (!user.isModified('password')) {
    return next();
  }
  bcrypt.genSalt(10, (err, salt) => {
    if (err) {
      return next(err);
    }
    bcrypt.hash(user.password, salt, (err, hash) => {
      if (err) {
        return next(err);
      }
      user.password = hash;
      next();
    });
  });
});

/**
 * Helper method for validating user's password.
 */
userSchema.methods.comparePassword = function (candidatePassword) {
  return comparePassword(candidatePassword, this.password);
};

autoIncrement.initialize(mongoose.connection);
userSchema.plugin(autoIncrement.plugin, {model: 'User', startAt: 1});
userSchema.plugin(mongooseUniqueValidator);

const User = mongoose.model('User', userSchema);

module.exports = User;
