const jwt = require('jsonwebtoken');

/**
 * Login Required middleware
 */
exports.isAuthenticated = (req, res, next) => {
  let token = req.body.token || req.query.token || req.headers['x-access-token'];
  if (token) {
    jwt.verify(token, process.env.JWT_SECRET, (err, decoded)=> {
      if (err) {
        res.status(401);
        return next(new Error('Authentication Failed'));
      } else {
        req.decoded = decoded;
        next();
      }
    });
  } else {
    res.status(403);
    return next(new Error('No token provided'));
  }
};

exports.isSiteAdmin = (req, res, next) => {
  let token = req.body.token || req.query.token || req.headers['x-access-token'];
  if (token) {
    jwt.verify(token, process.env.JWT_SECRET, (err, decoded)=> {
      if (err) {
        res.status(401);
        return next(new Error('Authentication Failed'));
      } else {
        if (decoded.user.role !== 'siteadmin') {
          res.status(401);
          return next(new Error('Unauthorized access'));
        }
        req.decoded = decoded;
        next();
      }
    });
  } else {
    res.status(403);
    return next(new Error('No token provided'));
  }
};
