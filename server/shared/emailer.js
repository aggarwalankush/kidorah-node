const nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport({
  service: process.env.EMAIL_SERVICE,
  auth: {
    user: process.env.EMAIL_USER,
    pass: process.env.EMAIL_PASSWORD
  }
});

transporter.verify(function (error, success) {
  if (error) {
    console.log(error);
  } else if (success) {
    console.log('Email Server is ready');
  } else {
    console.log('Problem in creating transporter');
  }
});

exports.sendMail = (to, subject, text, cb)=> {
  let mailOptions = {
    to: to,
    from: process.env.EMAIL_USER,
    subject: subject,
    text: text
  };
  if (cb) {
    transporter.sendMail(mailOptions, cb);
  } else {
    return transporter.sendMail(mailOptions);
  }
};
