const moment = require('moment');

exports.getFormattedDate = (ISODate)=> {
  return moment(ISODate).format('MM/DD/YYYY HH:mm')
};
