const auth = require('../shared/auth');
const userController = require('../controllers/user');
const teamController = require('../controllers/team');
const express = require('express');
const router = express.Router();
const teamRouter = express.Router();
const productRouter = express.Router();
/**
 * Primary app routes
 */
router.post('/user', auth.isSiteAdmin, userController.postUser);
router.post('/login', userController.postLogin);
router.post('/forgot', userController.postForgot);
router.route('/reset/:token')
  .get(userController.getReset)
  .post(userController.postReset);

/**
 * Team routes
 */
router.use('/team', teamRouter);
teamRouter.route('/')
  .get(teamController.getTeam)
  .post(teamController.postTeam);
teamRouter.get('/all', auth.isSiteAdmin, teamController.getAllTeams);
teamRouter.patch('/:id(\\d+)', auth.isSiteAdmin, teamController.patchTeam);

//TODO - DELETE THESE TEST APIs
//START
const testController = require('../controllers/test');
teamRouter.get('/:id(\\d+)', testController.getTeamById);
teamRouter.get('/:username(\\w+)', testController.getTeamByUser);

/**
 * Product routes
 */
router.use('/product', productRouter);
productRouter.post('/', testController.postProduct);
//END


module.exports = router;
