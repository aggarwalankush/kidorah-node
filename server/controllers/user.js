const async = require('async');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const User = require('../models/User');
const _ = require('lodash');
const emailer = require('../shared/emailer');

/**
 * POST /user
 * Creates a new user
 */
exports.postUser = (req, res, next) => {
  const user = new User({
    name: req.body.name || req.body.username,
    role: req.body.role || 'siteadmin',
    email: req.body.email,
    username: req.body.username,
    password: req.body.password
  });
  return user.save()
    .then(savedUser=> {
      return res.status(200).json({
        success: true,
        userId: savedUser._id
      });
    })
    .catch(err=>next(err));
};

/**
 * POST /login
 * Sign in using username and password
 */
exports.postLogin = (req, res, next) => {
  User.findOne({username: req.body.username})
    .then(user=> {
      if (!user) {
        res.status(401);
        throw new Error('Invalid username');
      }
      return user.comparePassword(req.body.password)
        .then(isMatch=> {
          if (isMatch != true) {
            res.status(401);
            throw new Error('Invalid password');
          }
          let token = jwt.sign({user: user}, process.env.JWT_SECRET, {
            expiresIn: process.env.JWT_TIMEOUT
          });
          return res.status(200).json({
            success: true,
            token: token,
            userId: user._id,
            role: user.role,
            name: user.name
          });
        });
    })
    .catch(err=>next(err));
};

/**
 * POST /forgot
 * Creates a random token and send user an email with a reset link.
 * @param {{forgotemail}} req.body
 */
exports.postForgot = (req, res, next) => {
  async.waterfall([
    function (done) {
      crypto.randomBytes(16, (err, buf) => {
        const token = buf.toString('hex');
        done(err, token);
      });
    },
    function (token, done) {
      User.findOne({email: req.body.forgotemail}, (err, user) => {
        if (!user) {
          res.status(401);
          return next(new Error('Account with that email address does not exist'));
        }
        user.passwordResetToken = token;
        user.passwordResetExpires = Date.now() + _.toNumber(process.env.RESET_TOKEN_TIMEOUT);
        user.save((err) => {
          done(err, token, user);
        });
      });
    },
    function (token, user, done) {
      let to = user.email;
      let subject = 'Reset your password on Kidorah';
      let text = 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
        'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
        process.env.HOST + ':' + process.env.PORT + '/reset/' + token + '\n\n' +
        'If you did not request this, please ignore this email and your password will remain unchanged.\n';
      emailer.sendMail(to, subject, text, function (err, response) {
        if (err) {
          done(err);
        } else {
          return res.status(200).json({
            success: true,
            userId: user._id
          });
        }
      });
    }
  ], (err) => {
    if (err) {
      return next(err);
    }
    res.send('done');
  });
};

/**
 * GET /reset/:token
 * Validate token
 */
exports.getReset = (req, res, next) => {
  User.findOne({passwordResetToken: req.params.token})
    .where('passwordResetExpires').gt(Date.now())
    .exec()
    .then(user=> {
      if (!user) {
        res.status(401);
        throw new Error('Password reset token is invalid or has expired');
      }
      return res.status(200).json({
        success: true,
        data: 'valid token'
      });
    })
    .catch(err=>next(err));
};

/**
 * POST /reset/:token
 * Reset user password
 */
exports.postReset = (req, res, next) => {
  User.findOne({passwordResetToken: req.params.token})
    .where('passwordResetExpires').gt(Date.now())
    .exec()
    .then(user=> {
      if (!user) {
        res.status(401);
        throw new Error('Password reset token is invalid or has expired');
      }
      user.password = req.body.password;
      user.passwordResetToken = undefined;
      user.passwordResetExpires = undefined;
      return user.save()
        .then(savedUser=> {
          let token = jwt.sign({user: savedUser}, process.env.JWT_SECRET, {
            expiresIn: process.env.JWT_TIMEOUT
          });
          return res.status(200).json({
            success: true,
            token: token,
            userId: savedUser._id
          });
        });
    })
    .catch(err=>next(err));
};
