const Team = require('../models/Team');
const User = require('../models/User');
const _ = require('lodash');
const moment = require('moment');
const utils = require('../shared/utils');
const Promise = require('bluebird');
const emailer = require('../shared/emailer');
const winston = require('winston');
const randomBytes = Promise.promisify(require('crypto').randomBytes);

/**
 * POST /team
 * Create a new team
 * @param {{teamname,teamadmin,teammember}} req.body
 */
exports.postTeam = (req, res, next) => {
  const team = new Team({
    name: req.body.teamname,
    admin_email: req.body.teamadmin,
    member_email: req.body.teammember
  });

  return team.save()
    .then(savedTeam=> {
      return res.status(200).json({
        success: true,
        teamId: savedTeam._id
      });
    })
    .catch(err=> next(err));
};

/**
 * GET /team
 * Get team by query params
 */
exports.getTeam = (req, res, next) => {
  let findQuery = {};
  _.forOwn(req.query, function (value, key) {
    switch (key) {
      case "name":
        _.set(findQuery, 'name', {$regex: new RegExp("^" + value + "$", "i")});
        break;
      case "id":
        _.set(findQuery, 'id', 'id');
        break;
    }
  });
  if (_.isEmpty(findQuery)) {
    res.status(400);
    return next(new Error('Missing/Invalid query params'));
  }
  return Team.findOne(findQuery)
    .then(existingTeam=> {
      if (!existingTeam) {
        res.status(204);
        throw new Error('Team not found');
      }
      return res.status(200).json({
        success: true,
        teamId: existingTeam._id
      });

    })
    .catch(err=> next(err));
};

/**
 * GET /team/all
 * Get all teams
 * Helps in pagination
 */
exports.getAllTeams = (req, res, next) => {
  return Team.paginate({
    "name": {"$regex": req.query.search || '', "$options": "i"},
    "status": req.query.status || 'new'
  }, {
    offset: +req.query.offset || 0,
    limit: +req.query.limit || 10,
    lean: false,
    sort: {createdAt: -1}
  }).then(teams=> {
    let teamsArr = [];
    _.forEach(teams.docs, function (team) {
      teamsArr.push({
        id: team._id,
        created: utils.getFormattedDate(team.createdAt),
        team_name: team.name,
        team_admin: team.admin_email,
        team_member: team.member_email,
        product_allowance: team.product_allowance
      });
    });
    return res.status(200).json({
      success: true,
      total: teams.total,
      teams: teamsArr
    });
  }).catch(err=> next(err));
};

/**
 * PATCH /team/:id
 * Approve or Reject Team
 * Update team info
 */
exports.patchTeam = (req, res, next) => {
  return Team.findOneAndUpdate({
    _id: req.params.id
  }, {
    status: req.body.status
  }, {
    new: true, // return the modified document rather than the original
    runValidators: true
  }).then(doc=> {
    if (doc.status === 'approved') {
      addNewTeamUsers(doc);
    }
    return res.status(200).json({
      success: true,
      teamId: doc._id
    });
  }).catch(err=> next(err));
};

const addNewTeamUsers = Promise.method(teamDoc=> {
  let executeParallel = [];
  executeParallel.push(addNewUser(teamDoc.admin_email, 'admin', teamDoc.name));
  _.forEach(teamDoc.member_email, email => {
    return executeParallel.push(addNewUser(email, 'member', teamDoc.name));
  });
  Promise.all(executeParallel)
    .then(data=> {
      winston.info(data);
    })
    .catch(err=> winston.error(err.message));
});

const addNewUser = Promise.method((email, role, teamname)=> {
  let emailName = _.split(email, '@', 1)[0];
  const user = new User({
    name: emailName,
    role: role,
    email: email,
    username: emailName
  });

  return User.findOne({email: email})
    .then(existingUser=> {
      if (existingUser) {
        return email + ' - Account already exists.';
      }
      return emailNewUser(user, teamname);
    })
    .catch(err=> email + ' - Error creating account');
});


const emailNewUser = Promise.method((newUser, teamname)=> {
  return randomBytes(16)
    .then(buf=> {
      return buf.toString('hex');
    })
    .then(token=> {
      newUser.passwordResetToken = token;
      newUser.passwordResetExpires = Date.now() + _.toNumber(process.env.RESET_TOKEN_TIMEOUT);
      return newUser.save()
        .then(savedUser=> {
          let to = savedUser.email;
          let subject = 'Kidorah Team Approved. Create Password to login';
          let text = 'You are receiving this because you are a member of a newly created team - ' + teamname + '\n\n' +
            'Username for login - ' + savedUser.username + '\n\n' +
            'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
            process.env.HOST + ':' + process.env.PORT + '/reset/' + token + '\n\n';
          return emailer.sendMail(to, subject, text);
        })
        .then(emailResponse=> email + ' - Account created');
    });
});
